Simulate OFDM and GMSK signals together
=======================================

In this project, there are simulations of simple 4PSK 64 tone OFDM and a GMSK signals.
These signals are mixed and their interference results in BER.

Manual simulation
-----------------

- `ber_sim.slx` simulink schema for manual simulation
- `myplot.m` make plots of manual simulation results
- `plot_fft.m` function averaging FFT windows for prettier graph
- `prepare_constants.m` prepare parameters for manual simulation

Automatic Simulation
--------------------

- `parallel_ber_sim_model.slx` simulink schema for automated simulations
- `parallel_ber_sim.m` make automated simulations for different signal strengths
- `parallel_ber_sim_zeros.m` cover a special case forgotten by initial `parallel_ber_sim.m`, now obsolete
- `process_results.m` load measured data and make plots
- `parallel_ber_sim_plus.m` automated simulations of larger range
- `process_results_new.m` load measured data and do more plots, this is addition in 2022 for different fft sizes

