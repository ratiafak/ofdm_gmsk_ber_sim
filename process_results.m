
%% Load
load('results_20200127_080514.mat');    %Load a set of  gmsk power / ofdm power / number of empy tones

db_ofdm = log10(range_ofdm_power)*10;
db_gmsk = log10(range_gmsk_power)*10;


%% BER map compare strengths of both signals
f = figure(1);
pl_globallinewidth = 0.5;

for empty_idx = 1:4
    if(empty_idx < 4)
        subplot(4, 3, empty_idx);
    else
        subplot(4, 3, 4:12);
    end

    empty_tones_idx = empty_idx;    %Chart for range_empty_tones(empty_tones_idx)
    maxber = -2;    %Maximal BER displayed
    minber = -6;    %Minimal BER displayed

    %Number of bits transmitted on all tones
    bits_ofdm = symbols * 2 ...
        * (size(output_ofdm_errors, 1) - range_empty_tones(empty_tones_idx));

    %Preallocate map
    map = zeros([length(db_gmsk)-1 length(db_ofdm)-1 3]);

    for gmsk_idx = 2:length(db_gmsk)
        for ofdm_idx = 2:length(db_ofdm)
            %GMSK BER
            ber_gmsk = log10(output_gmsk_errors(gmsk_idx, ofdm_idx, empty_tones_idx)/symbols);

            %OFDM BER, all channels
            ber_ofdm = log10(sum(output_ofdm_errors(:, gmsk_idx, ofdm_idx, empty_tones_idx))/bits_ofdm);

            ber_gmsk = min(maxber, ber_gmsk);
            ber_gmsk = max(minber, ber_gmsk);
            ber_ofdm = min(maxber, ber_ofdm);
            ber_ofdm = max(minber, ber_ofdm);

            map(gmsk_idx-1, ofdm_idx-1, :) = ...
                [1-(maxber-ber_gmsk)/(maxber-minber) 1-(maxber-ber_ofdm)/(maxber-minber) 1];
        end
    end

    % Draw image
    image(db_ofdm(2:end), db_gmsk(2:end), map);
    set(gca,'YDir','normal');

    % Plot improvements
    if(range_empty_tones(empty_tones_idx) == 0)
        title('No empty tones');
    elseif(range_empty_tones(empty_tones_idx) == 1)
        title('1 empty tone');
    else
        title([num2str(range_empty_tones(empty_tones_idx)) ' empty tones']);
    end
    ylabel('GMSK power [dB]');
    xlabel('OFDM power [dB]');
    xtickangle(0);
    grid('on');
    if(empty_idx == 4)
        hold('on');
        plot(nan, nan, 's', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [1 1 1]);
        plot(nan, nan, 's', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [0 1 1]);
        plot(nan, nan, 's', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [1 0 1]);
        plot(nan, nan, 's', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [0 0 1]);
        legend(['BER > 10^{' num2str(maxber) '}'], ...
            ['GMSK BER < 10^{' num2str(minber) '}'], ...
            ['OFDM BER < 10^{' num2str(minber) '}'], ...
            ['BER < 10^{' num2str(minber) '}'], 'Location', 'northwest');
        hold('off');
        xticks(db_ofdm(2):5:db_ofdm(end));
        yticks(db_gmsk(2):5:db_gmsk(end));
    else
        xticks(db_ofdm(2):10:db_ofdm(end));
        yticks(db_gmsk(2):10:db_gmsk(end));
    end
    set(gca, 'Linewidth', pl_globallinewidth);
end

% Print
set(gcf,'PaperPositionMode','auto')
f.PaperUnits = 'centimeters';
f.PaperPosition = [0 0 16 21];
print(f, '-depsc', 'fig_map.eps');

%% Fixed OFDM power how hole size affects GMSK
f = figure(2);
pl_globallinewidth = 0.5;

semilogy(nan, nan, 'x', 'Color', 'none');   % Empty for legend title
hold('on');
semilogy(nan, nan, 'x', 'Color', 'none');
max_empty_tones_idx = 5;    %Print ber for first n from range_empty_tones
line_colors = parula(max_empty_tones_idx+1)*0.8;

for gmsk_idx = 1:max_empty_tones_idx
    ofdm_0_idx = find(db_ofdm == 0, 1);
    errors = output_gmsk_errors(:, ofdm_0_idx, gmsk_idx)/symbols; %BER
    
    semilogy(db_gmsk, errors, 'x-', 'Color', line_colors(gmsk_idx, :), 'Linewidth', 1);
end

% Print extra case without OFDM
errors = output_gmsk_errors(:, 1, 1)/symbols; %BER
semilogy(db_gmsk, errors, 'x-', 'Color', line_colors(end, :), 'Linewidth', 1);
hold('off');

% Plot improvements
legend(cat(2, {'empty' 'tones'},...
    cellfun(@num2str,num2cell(range_empty_tones(1:max_empty_tones_idx)),'un',0),...
    {'no OFDM'}), 'Location', 'best');
set(gca, 'Linewidth', pl_globallinewidth);
grid('on');
xlabel('GMSK Signal Power [dB]');
ylabel('BER');

% Print
f.PaperUnits = 'centimeters';
f.PaperPosition = [0 0 16 11];
print(f, '-depsc', 'fig_gmsk.eps');


%% Fixed GMSK power, how tone errors rise near GMSK
f = figure(3);
pl_globallinewidth = 0.5;

semilogy(nan, nan, 'x', 'Color', 'none');   % Empty for legend title
hold('on');
max_tone = 4;    %Print ber for tones near GMSK
tone0 = find(output_ofdm_errors(:, 2, 2, 2) == 0);  %Find the tone where GMSK is centered
line_colors = parula(max_tone+2)*0.8;

for gmsk_idx = 0:max_tone
    gmsk_0_idx = find(db_gmsk == 0, 1);
    bits_ofdm = symbols * 2 * 2;    %Number of bits transmitted on two tones with the same distance from tone0
    errors = permute(sum(output_ofdm_errors([tone0+gmsk_idx tone0-gmsk_idx], gmsk_0_idx, :, 1), 1), [3 2 1])/bits_ofdm; %BER
    
    semilogy(db_ofdm, errors, 'x-', 'Color', line_colors(gmsk_idx+1, :), 'Linewidth', 1);
end

% Print extra case without GMSK
bits_ofdm = symbols * 2 * (size(output_ofdm_errors, 1) - range_empty_tones(1));    %Number of bits transmitted on all tones
errors = permute(sum(output_ofdm_errors(:, 1, :, 1), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x-', 'Color', line_colors(end, :), 'Linewidth', 1);
hold('off');

% Plot improvements
legend(cat(2, {'tone'},...
    cellfun(@num2str,num2cell(0:max_tone),'un',0),...
    {'all, no GMSK'}), 'Location', 'best');
set(gca, 'Linewidth', pl_globallinewidth);
grid('on');
xlabel('OFDM Signal Power [dB]');
ylabel('BER');
xlim([-5 25]);

% Print
f.PaperUnits = 'centimeters';
f.PaperPosition = [0 0 16 11];
print(f, '-depsc', 'fig_ofdm.eps');

