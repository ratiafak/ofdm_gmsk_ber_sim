
%% Simulation range
% Power of noise is 1
% Power of OFDM would be 1 if all 64 tones were used
%    some are removed as guarbands, some are empty tones

% This script runs mesh to make a map

range_ofdm_power = [0 10.^(( -20:1:30 )/10)];   % Simulate this range of power for OFDM
range_gmsk_power = [0 10.^(( -20:1:30 )/10)];   % Simulate this range of power for GMSK

symbols = 1000000;  % Simulate this many symbols, GMSK - number of bits, OFDM - symbol is 2 bits times number of tones

% Create vector of meshgrid with all parameters
[input_ofdm_power, input_gmsk_power] = meshgrid(range_ofdm_power, range_gmsk_power);
sim_n = numel(input_ofdm_power);
input_ofdm_power = reshape(input_ofdm_power, [sim_n 1]);
input_gmsk_power = reshape(input_gmsk_power, [sim_n 1]);

disp(['Simulate ' num2str(sim_n) ' times for ' num2str(empty) ' empties, ' num2str(n_fft) ' size FFT']);

%% Prepare the model
disp(['Configuring parameters for ' num2str(empty) ' empties']);

model = 'parallel_ber_sim_model';
load_system(model);

simin = Simulink.SimulationInput(model);   % Preinitialize input structures
simin = simin.setModelParameter('SimulationMode', 'rapid-accelerator');
simin = simin.setModelParameter('StopTime', num2str(symbols));
simin = simin.setVariable('empty_tones', empty, 'Workspace', 'parallel_ber_sim_model');
simin = simin.setVariable('n_fft', n_fft, 'Workspace', 'parallel_ber_sim_model');

% Copy model and set specific values
simin = repmat(simin, [sim_n 1]);
for idx = 1:sim_n
    simin(idx) = simin(idx).setVariable('ofdm_power', input_ofdm_power(idx), 'Workspace', 'parallel_ber_sim_model');
    simin(idx) = simin(idx).setVariable('gmsk_power', input_gmsk_power(idx), 'Workspace', 'parallel_ber_sim_model');
end

%% Simulate
disp('Simulate...');
simout = parsim(simin, 'ShowSimulationManager', 'off');

%% Store results
tones_n = length(simout(1).ofdm_errors);
output_ofdm_errors = reshape([simout.ofdm_errors], [tones_n length(range_ofdm_power) length(range_gmsk_power)]);
output_gmsk_errors = reshape([simout.gmsk_errors], [length(range_ofdm_power) length(range_gmsk_power)]);
save(['results_e' num2str(empty) '_f' num2str(n_fft) '_' char(datetime('now','Format','yyyyMMdd_HHmmss'))], 'symbols', 'range_ofdm_power', 'range_gmsk_power', 'empty', 'output_ofdm_errors', 'output_gmsk_errors');
disp(['Done for ' num2str(empty) ' empties']);

