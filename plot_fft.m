function ffsig = plot_fft(signal)
%Plot spectrum of signal

if 1
    ffsigsize = 1024;   %Size of one fft frame
    framecount = floor(length(signal)/ffsigsize);  %Number of fft frames
    ffsig = zeros([ffsigsize 1]);
    for frame = reshape(signal(1:ffsigsize*framecount), ffsigsize, framecount)
        %ffsig = ffsig + abs(fft(hamming(framesize) .* frame)) / framecount;
        ffsig = ffsig + abs(fft(frame)) / framecount;
    end
else
    ffsig = abs(fft(signal));
    ffsigsize = length(signal);
end

ffsig = [ffsig(ffsigsize/2+1:end); ffsig(1:ffsigsize/2)];
ffsig = (ffsig / ffsigsize).^2;
ffsig = 10*log10(ffsig);

