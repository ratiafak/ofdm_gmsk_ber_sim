
%% Simulation range
% Power of noise is 1
% Power of OFDM would be 1 if all 64 tones were used
%    some are removed as guarbands, some are empty tones
range_ofdm_power = [0 10.^(( -10:1:20 )/10)];   % Simulate this range of power for OFDM
range_gmsk_power = [0 10.^(( -20:1:10 )/10)];   % Simulate this range of power for GMSK
range_empty_tones = [0 1:2:11]; % Simulate while removing this many tones
symbols = 1000000;  % Simulate this many symbols, GMSK - number of bits, OFDM - symbol is 2 bits times number of tones

% Create vector of meshgrid with all parameters
[input_ofdm_power, input_gmsk_power, input_empty_tones] = meshgrid(range_ofdm_power, range_gmsk_power, range_empty_tones);
sim_n = numel(input_ofdm_power);
input_ofdm_power = reshape(input_ofdm_power, [sim_n 1 1]);
input_gmsk_power = reshape(input_gmsk_power, [sim_n 1 1]);
input_empty_tones = reshape(input_empty_tones, [sim_n 1 1]);

disp(['Simulate ' num2str(sim_n) ' times']);

%% Prepare the model
% Build the model
disp('Building model...');
model = 'parallel_ber_sim_model';
load_system(model);
Simulink.BlockDiagram.buildRapidAcceleratorTarget(model);
simin = Simulink.SimulationInput(model);   % Preinitialize input structures
simin = simin.setModelParameter('SimulationMode', 'rapid-accelerator');
simin = simin.setModelParameter('RapidAcceleratorUpToDateCheck', 'off');
simin = simin.setModelParameter('StopTime', num2str(symbols));

% Copy model and set specific values
disp('Configuring parameters...');
simin = repmat(simin, [sim_n 1]);
for idx = 1:sim_n
    simin(idx) = simin(idx).setVariable('ofdm_power', input_ofdm_power(idx));
    simin(idx) = simin(idx).setVariable('gmsk_power', input_gmsk_power(idx));
    simin(idx) = simin(idx).setVariable('empty_tones', input_empty_tones(idx));
end

%% Simulate
disp('Simulate...');
simout = parsim(simin, 'ShowSimulationManager', 'off');

%% Store results
tones_n = length(simout(1).ofdm_errors);
output_ofdm_errors = reshape([simout.ofdm_errors], [tones_n length(range_ofdm_power) length(range_gmsk_power) length(range_empty_tones)]);
output_gmsk_errors = reshape([simout.gmsk_errors], [length(range_ofdm_power) length(range_gmsk_power) length(range_empty_tones)]);
save(['results_' char(datetime('now','Format','yyyyMMdd_HHmmss'))], 'symbols', 'range_ofdm_power', 'range_gmsk_power', 'range_empty_tones', 'output_ofdm_errors', 'output_gmsk_errors');
