#!/bin/bash

~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty = -1; run('parallel_ber_sim_cross.m'); quit"
~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty =  0; run('parallel_ber_sim_cross.m'); quit"
~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty =  1; run('parallel_ber_sim_cross.m'); quit"
~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty =  3; run('parallel_ber_sim_cross.m'); quit"
~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty =  5; run('parallel_ber_sim_cross.m'); quit"
~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty =  7; run('parallel_ber_sim_cross.m'); quit"
~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty =  9; run('parallel_ber_sim_cross.m'); quit"
~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty = 11; run('parallel_ber_sim_cross.m'); quit"
~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty = 13; run('parallel_ber_sim_cross.m'); quit"
~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty = 15; run('parallel_ber_sim_cross.m'); quit"
~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty = 17; run('parallel_ber_sim_cross.m'); quit"
~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty = 19; run('parallel_ber_sim_cross.m'); quit"
~/.MATLAB/R2021b/bin/matlab -nodisplay -nosplash -sd ~/Engineering/ofdm_fhss_ber_sim -r "empty = 21; run('parallel_ber_sim_cross.m'); quit"

